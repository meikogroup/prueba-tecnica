# Prueba de desarrollo Grupo Meiko

Bienvenido a la prueba técnica de Grupo Meiko, el objetivo de la misma es que puedas demostrar tus capacidades a la hora de codificar y de adaptarte a diferentes tecnologias.

## Requisitos iniciales:

* Puedes usar cualquier framework tanto para back como para front.

* Usa lenguaje [PHP](https://www.php.net/) para tu prueba.

* Puedes usar cualquier libreria o framework para front, pero es importante que uses tecnologias que permitan SPA, preferiblemente usa [Angular](https://cli.angular.io/) ya que próximos desarrollos estarán enfocados a dicha tecnologia.

* Al finalizar la prueba envía correo a `david.perez@grupomeiko.com` con el repositorio de tu prueba ó haz un pull request aquí.

* Recuerda hacer el readme correctamente con las instrucciones para ejecutar tu código.

## Requerimientos:

Grupo Meiko requiere desarrollar un sistema de usuarios nuevo con los siguientes perfiles:

* Administrador.
* Usuario.
* Grupos de trabajo.

Los usuarios pertenecen a grupos de trabajo, por tanto es importante que el administrador pueda crear, editar, borrar y asignar usuarios a estos grupos de trabajo.

El administrador es el único que puede crear usuarios, actualizarlos, borrarlos, cambiarlos de grupo de trabajo y ver sus perfiles.

Los usuarios solamente pueden acceder a su propio perfil y modificarlo pero no pueden crear otros usuarios, así mismo, pueden ver a que grupo pertenecen pero no pueden cambiarlo.


## Forma de calificación:

* Que tu código funcione y cumpla con los requerimientos.

* Arquitectura enfocada a microservicios.

* Uso correcto de tecnologias back y front.

* Se califica tu dominio con GIT y el correcto versionamiento.

* Caracteristicas adicionales al problema planteado (JWT, Manejo de paquetes NPM etc..).

* Si tu aplicación está dockerizada suma muchos puntos.

**Ánimo y muchos éxitos!**